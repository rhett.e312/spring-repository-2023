using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BlockColor
{
    Green,
    Red
}

public class Block : MonoBehaviour
{
   public BlockColor color;
   
   public GameObject brokenBlockLeft;
   public GameObject brokenBlockRight;
   public float brokenBlockForce;
   public float brokenBlockTorque;
   public float brokenBlockDestroyDelay;

    void OnTriggerEnter (Collider other)
    {
        if(other.CompareTag("SwordRed"))
        {
            if(color == BlockColor.Red)
            {
                GameManager.instance.AddScore();
            }
            else
            {
                GameManager.instance.HitWrongBlock();
            }
            Hit();
        }
        else if(other.CompareTag("SwordGreen"));
        {
            if(color == BlockColor.Green)
            {
                GameManager.instance.AddScore();
            }
            else
            {
                GameManager.instance.HitWrongBlock();
            }
            Hit();
        }
    }
   void Hit ()
   {
        // enable the broken pieces
        brokenBlockLeft.SetActive(true);
        brokenBlockRight.SetActive(true);

        // removes them as children
        brokenBlockLeft.transform.parent = null;
        brokenBlockRight.transform.parent = null;

        // add force to them
        Rigidbody RightRig = brokenBlockRight.GetComponent<Rigidbody>();
        Rigidbody LeftRig = brokenBlockLeft.GetComponent<Rigidbody>();

        LeftRig.AddForce(-transform.right * brokenBlockForce, ForceMode.Impulse);
        RightRig.AddForce(-transform.right * brokenBlockForce, ForceMode.Impulse);

        // add tourque to them
        LeftRig.AddTorque(-transform.forward * brokenBlockTorque, ForceMode.Impulse);
        RightRig.AddTorque(-transform.forward * brokenBlockTorque, ForceMode.Impulse);

        // destroy the broken pieces after a few seconds
        Destroy(brokenBlockLeft, brokenBlockDestroyDelay);
        Destroy(brokenBlockRight, brokenBlockDestroyDelay);
   }
}
